/*
    SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

// application headers
#include "translatorwindow.h"

#include "translatordebug.h"

// KF headers
#include <KActionCollection>
#include <KConfigDialog>
#include <TextTranslator/TranslatorWidget>

TranslatorWindow::TranslatorWindow()
    : KXmlGuiWindow()
    , m_translatorWidget(new TextTranslator::TranslatorWidget(this))
{
    setCentralWidget(m_translatorWidget);
    m_translatorWidget->setStandalone(false);
    m_translatorWidget->show();

    KStandardAction::quit(qApp, SLOT(closeAllWindows()), actionCollection());

    setupGUI(Keys | Save | Create);
}

TranslatorWindow::~TranslatorWindow()
{
}

#include "moc_translatorwindow.cpp"
