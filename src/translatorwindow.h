/*
    SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#ifndef TRANSLATORWINDOW_H
#define TRANSLATORWINDOW_H

#include <KXmlGuiWindow>

namespace TextTranslator {
class TranslatorWidget;
}

/**
 * This class serves as the main window for translator.  It handles the
 * menus, toolbars and status bars.
 *
 * @short Main window class
 * @author Carl Schwan <carl@carlschwan.eu>
 * @version 1.0
 */
class TranslatorWindow : public KXmlGuiWindow
{
    Q_OBJECT
public:
    /**
     * Default Constructor
     */
    TranslatorWindow();

    /**
     * Default Destructor
     */
    ~TranslatorWindow() override;

private:
    TextTranslator::TranslatorWidget *m_translatorWidget;
};

#endif // TRANSLATORWINDOW_H
