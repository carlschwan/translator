# KDE Translator

A translator application that supports multiple services:

- Google translate
- Bing
- Deepl
- Libre Translate
- Lingva
- Yandex

![](screenshot.png)
